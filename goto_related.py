import os 
import sublime
import sublime_plugin
import os.path
import re
import glob
import os
import fnmatch

class AbstractGotoCommand(sublime_plugin.WindowCommand):
    
    input = None
    input_callback = None

    select_options = None
    select_callback = None
    highlighted_callback = None
    
    def show_select(self, options, callback = None, on_highlighted = None):
        if len(options) > 0:
            self.select_callback = callback
            self.highlighted_callback = on_highlighted
            self.select_options = options
            self.select = self.window.show_quick_panel(self.select_options, self._on_select_done, 0, -1, self._on_highlighted)

    def _on_select_done(self, index):
        if self.select_callback != None:
            value = self.select_options[index]
            self.select_callback(index, value)

    def _on_highlighted(self, index):
        if self.highlighted_callback != None:
            value = self.select_options[index]
            self.highlighted_callback(index, value)

    def show_input(self, label, default, callback):
        self.input_callback = callback
        self.input = self.window.show_input_panel(
            label,
            default,
            self.on_input_done,
            self.on_input_change,
            self.on_input_cancel
        )

    def on_input_done(self, value):
        if self.select_callback != None:
            self.select_callback(index, value)
        pass

    def on_input_change(self, value):
        pass

    def on_input_cancel(self):
        pass

    
class GotoRelatedCommand(AbstractGotoCommand):
    
    def run(self):

        self.base_dir = ''
        self.initial_view = self.window.active_view()
        self.filepath = self.window.active_view().file_name();

        folders = self.window.folders()
        for folder in folders:
            if self.filepath.find(folder) == 0:
                self.filepath = self.filepath.replace(folder + '/', "")
                os.chdir(folder)
                self.base_dir = folder + '/'
                break

        self.filename, self.file_extension = os.path.splitext(self.filepath)
        self.filename =  os.path.basename(self.filepath)
        self.file_extension = self.file_extension[1:]
        self.file_basename =  os.path.basename(self.filename)
        self.file_dir = os.path.dirname(self.filepath)

        # print('[FILE_NAME]', self.filename)
        # print('[FILE_BASENAME]', self.file_basename)
        # print('[FILE_PATH]', self.filepath)
        # print('[FILE_DIR]', self.file_dir)
        # print('[FILE_EXT]', self.file_extension)

        pattern = [
        # rails
        {
            'match': [
                'app/controllers/(.*)s_controller\.rb',
                'app/views/(.*)s/.*\.erb',
                'app/models/(.*)\.rb',
            ],
            'pattern': [
                'app/controllers/[MATCH:1]s_controller.rb',
                'app/views/[MATCH:1]s/*.erb',
                'app/models/[MATCH:1].rb',
            ]
        },
        # tpz
        {
            'match': [
                'app/(.*)/controller/(edit|list)(.*)\.class\.php',
                'app/(.*)/view/(edit|list)(.*)\.php',
                'app/(.*)/(assets)/js/view/(.*)\.js'
            ],
            'pattern': [
                'app/[MATCH:1]/controller/*[MATCH:3].class.php',
                'app/[MATCH:1]/view/*[MATCH:3].php',
                'app/[MATCH:1]/assets/js/view/*[MATCH:3].js',
                'model/[MATCH:3].class.php',
            ]
        },{
            'match': [
                'model/(.*)\.class\.php'
            ],
            'pattern': [
                'app/*/controller/*[MATCH:1].class.php',
                'app/*/view/*[MATCH:1].php',
                'app/*/assets/js/view/*[MATCH:1].js',
            ]
        },]

        files = []
        for item in pattern:
            m = self.is_matching(item['match'], self.filepath)
            if m:
                files = files + self.get_files(item['pattern'], m)

        files = sorted(list(set(files)));
        files = [[os.path.basename(file), file] for file in files if file != self.filepath]

        if(len(files) > 0):
            self.show_select(files, self.on_file_select, self.on_highlighted)

        else:
            # Fallback show goto panel
            base = self.filename.split(".")[0]
            base = re.sub("^[^a-zA-Z0-9]*", "", base)
            base = re.sub("[^a-zA-Z0-9]*$", "", base)
            self.window.run_command("show_overlay", {"overlay":"goto", "text": base})

    def get_files(self, pattern, m):
        if isinstance(pattern, list):
            files = []
            for p in pattern:
                files = files + self.get_files(p, m)
            return files
        else:
            l = len(m.groups())

            p = pattern
            p = p.replace('[FILE_NAME]', self.filename)
            p = p.replace('[FILE_BASENAME]', self.file_basename)
            p = p.replace('[FILE_PATH]', self.filepath)
            p = p.replace('[FILE_DIR]', self.file_dir)
            p = p.replace('[FILE_EXT]', self.file_extension)

            for match_no in re.findall('\[MATCH:(\d+)\]', p):
                match_no_i = int(match_no)
                if match_no_i <= l:
                    p = p.replace('[MATCH:' + match_no + ']', m.group(match_no_i))
            
            return glob.glob(p)


    def is_matching(self, match, str):
        if isinstance(match, list):
            for mat in match:
                m = self.is_matching(mat, str)
                if m:
                    return m
        else:
            return re.match(match, str)
        return False

    def on_file_select(self, index, value):
        if index >= 0:
            self.window.open_file(self.base_dir + value[1])
        elif self.initial_view:
            self.window.focus_view(self.initial_view)

    def on_highlighted(self, index, value):
        self.window.open_file(self.base_dir + value[1], sublime.TRANSIENT)

